# Лабораторная работа 1

**Название:** "Разработка драйверов символьных устройств"

**Цель работы:** "получить знания и навыки разработки драйверов символьных устройств для операционной системы Linux."

## Описание функциональности драйвера

Драйвер реализует парсинг входящей строки на числа. После чего суммирует все числа, который удалось распарсить и возвращает итоговую сумму
## Инструкция по сборке

сборка и загрузка драйвера
cd /ch_drv
make
sudo insmod ch_drv.ko

выгрузка драйвера
sudo rmmod ch_drv.ko

## Инструкция пользователя

1. Пользователю необходимо каким-либо образом записать в файл /dev/mychdev строку, включающую числа. В самом простом случае, можно воспользоваться стандартными средствами shell (echo <string> > /dev/mychdev)
2. Считать данные из файла /dev/mychdev строку, в которой находится итоговая сумма. Например (сat /dev/mychdev)

## Примеры использования

root@kurtlike-VirtualBox:/dev# cat mychdev

0

root@kurtlike-VirtualBox:/dev# echo 1 > mychdev

root@kurtlike-VirtualBox:/dev# cat mychdev

1

root@kurtlike-VirtualBox:/dev# echo 1a12d234sf > mychdev

root@kurtlike-VirtualBox:/dev# cat mychdev

247

root@kurtlike-VirtualBox:/dev# echo 1000a500bb > mychdev

root@kurtlike-VirtualBox:/dev# cat mychdev

1500

